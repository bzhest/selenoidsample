package org.selenide.examples.selenoid.browsers;

import com.codeborne.selenide.WebDriverProvider;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;

import static org.selenide.examples.selenoid.BaseTest.getCurrentTime;

public class CustomSelenoidFirefoxDriver implements WebDriverProvider {

    @Override
    public WebDriver createDriver(DesiredCapabilities capabilities) {
        DesiredCapabilities browser = new DesiredCapabilities();
        capabilities.acceptInsecureCerts();
        //browser.setCapability(ChromeOptions.CAPABILITY, CustomIncognitoDriver.getChromeOptions());
        browser.setBrowserName("firefox");
        browser.setVersion("83.0");
        browser.setCapability("enableVNC", true);
        browser.setCapability("enableLog", hasLogs());
        browser.setCapability("enableVideo", hasVideo()); //records browser session video to http://35.235.98.135:4444/video/
        browser.setCapability("videoName", getCurrentTime()); //set up custom video name
        capabilities.setCapability("videoFrameRate", 24);

        try {
            RemoteWebDriver driver = new RemoteWebDriver(
                    URI.create(System.getProperty("selenoidUrl") + "/wd/hub").toURL(),
                    browser);

            driver.setFileDetector(new LocalFileDetector()); // it needs to access to files (like images on machine)
            driver.manage().window().setSize(new Dimension(1280, 800));
            return driver;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public Boolean hasVideo(){
        return System.getProperty("videoMode").equals("true");
    }

    public Boolean hasLogs(){
        return System.getProperty("logsMode").equals("true");
    }



}
