package org.selenide.examples.selenoid.browsers;

import com.codeborne.selenide.WebDriverProvider;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;

import static org.selenide.examples.selenoid.BaseTest.getCurrentTime;

public class CustomSelenoidChromeDriver implements WebDriverProvider {

    @Override
    public WebDriver createDriver(DesiredCapabilities capabilities) {
        DesiredCapabilities browser = new DesiredCapabilities();
        capabilities.acceptInsecureCerts();
        browser.setCapability(ChromeOptions.CAPABILITY, getChromeOptions());
        browser.setBrowserName("chrome");
        browser.setVersion("86.0");
        browser.setCapability("enableVNC", true);
        browser.setCapability("enableLog", hasLogs());
        browser.setCapability("enableVideo", hasVideo()); //records browser session video to http://34.68.121.7:4444/video/
        browser.setCapability("videoName", getCurrentTime()); //set up custom video name

        capabilities.setCapability("videoFrameRate", 24);

        try {
            RemoteWebDriver driver = new RemoteWebDriver(
                    URI.create(System.getProperty("selenoidUrl") + "/wd/hub").toURL(),
                    browser);

            driver.setFileDetector(new LocalFileDetector()); // it needs to access to files (like images on machine)
            driver.manage().window().setSize(new Dimension(1280, 800));
            return driver;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static ChromeOptions getChromeOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");

        options.setHeadless(isHeadless());
        options.setExperimentalOption("useAutomationExtension", false);
        //options.addArguments("headless","disable-gpu");//since it's custom driver, this String make's tests run in headless (Configuration.headless = true -> do it with default created browser)

        return options;
    }

    public static Boolean isHeadless() {
        String environmentSomeOption = System.getProperty("headlessMode");
        return environmentSomeOption.equals("true");
    }

    public Boolean hasVideo(){
        return System.getProperty("videoMode").equals("true");
    }

    public Boolean hasLogs(){
        return System.getProperty("logsMode").equals("true");
    }

}
