package org.selenide.examples.selenoid.browsers;

import com.codeborne.selenide.WebDriverProvider;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomIncognitoDriver implements WebDriverProvider {
    protected final static Logger logger = LoggerFactory.getLogger(CustomIncognitoDriver.class);


    @Override
    public WebDriver createDriver(DesiredCapabilities capabilities) {
        capabilities.acceptInsecureCerts();
        capabilities.setCapability(ChromeOptions.CAPABILITY, getChromeOptions());
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);//about a problem with 'self-signed' sertificat - http://ru.selenide.org/2015/06/21/selenide-2.19/
        ChromeDriverManager.chromedriver().setup();// .getInstance().setup();
        return new ChromeDriver();
    }

    public static ChromeOptions getChromeOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");

        options.setHeadless(isHeadless());
        options.setExperimentalOption("useAutomationExtension", false);
        options.addArguments("headless","disable-gpu");//since it's custom driver, this String make's tests run in headless (Configuration.headless = true -> do it with default created browser)

        return options;
    }

    public static Boolean isHeadless() {
        String environmentSomeOption = System.getProperty("headlessMode");
        return environmentSomeOption.equals("true");
    }

}
