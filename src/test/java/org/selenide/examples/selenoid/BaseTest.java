package org.selenide.examples.selenoid;

import com.codeborne.selenide.Configuration;
import org.selenide.examples.selenoid.browsers.CustomIncognitoDriver;
import org.selenide.examples.selenoid.browsers.CustomSelenoidChromeDriver;
import org.selenide.examples.selenoid.browsers.CustomSelenoidFirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


//import static com.codeborne.selenide.Configuration.AssertionMode.STRICT;
//import static com.codeborne.selenide.Configuration.FileDownloadMode.PROXY;

//Command + T - possible to make rebase (similar to go git pull origin master)

public class BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @BeforeSuite
    public void beforeSuite() {
        //параллелить тесты по классам mvn -Dparallel=classes test

        Configuration.browser = whereToRun();

        Configuration.browserSize = "1280x800";
        Configuration.timeout = 7000;
        Configuration.reportsFolder = "target/test-result/results";
        Configuration.baseUrl = "https://www.epam.com/";
    }


    public String whereToRun() {

        switch (System.getProperty("placeToRunMode")) {
            case ("locallyChrome"):
                return CustomIncognitoDriver.class.getName();
            case ("selenoidChrome"):
                return CustomSelenoidChromeDriver.class.getName();
            case ("selenoidFirefox"):
                return CustomSelenoidFirefoxDriver.class.getName();
            default:
                return CustomIncognitoDriver.class.getName();
        }
    }

    //'headlessMode' is set from pom.xml
    public static Boolean isHeadless() {
        String environmentSomeOption = System.getProperty("headlessMode");
        return environmentSomeOption.equals("true");
    }

    public static String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MMMMM_dd__hh_mm_aaa");
        String formattedDate = dateFormat.format(date);
        return formattedDate;
    }
}
