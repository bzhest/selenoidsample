package org.selenide.examples.selenoid.tests;

import org.openqa.selenium.By;
import org.selenide.examples.selenoid.BaseTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.Assertions.assertThat;

public class SecondTest extends BaseTest {

  @Test
  void secondTestOk() {
    open("insights");
    assertThat(false).isFalse();
  }

  @Test
  void secondTestFail() {
    open("insights");
    assertThat(true).isFalse();
  }

  @Test
  void secondTestBroken() {
    open("insights");
    $(By.cssSelector("#werwerwer")).click();
    assertThat(true).isFalse();
  }

}
